package GUI;

import Principal.ProyectoIIParcial;
import static Principal.ProyectoIIParcial.sceneVentanaHotelesII;
import static Principal.ProyectoIIParcial.window;
import generadorhtmlmapbox.GeneradorHTMLMapBox;
import generadorhtmlmapbox.Ubicacion;
import geocoding.GeoCoding;
import java.awt.Desktop;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import recopilardatos.Ciudad;
import recopilardatos.Hotel;
import recopilardatos.Provincia;

/**
 * Esta clase muestra la ventanaI para buscar hoteles
 *
 * @author Grupo8
 */
public class VentanaHotelesI {

    private VBox root = new VBox();

    private TableView<Hotel> table = new TableView<Hotel>();
    private static ArrayList<Hotel> hotelesCiudad = new ArrayList<>();
    Map<Double, Hotel> mapa = new TreeMap<>();

    private ArrayList<Hotel> hotelesProximos = new ArrayList<>();

    private ObservableList<Hotel> data;
    private Label labelProvincias = new Label("Seleccione la provincia: ");
    private HBox panelProvincias = new HBox();
    private ComboBox comboProvincias = new ComboBox();

    private Label labelCiudades = new Label("Seleccione la ciudad: ");

    private HBox panelCiudades = new HBox();
    private ComboBox comboCiudades = new ComboBox();

    private Button busquedaProximidad = new Button("Búsqueda por proximidad");
    private Button busquedaProvincias = new Button("Búsqueda por provincias y ciudades");

    private Label labelTipoBusqueda = new Label("Seleccione el tipo de búsqueda");

    private static ArrayList<Ciudad> ciudadesComboBox = new ArrayList<>();

    private static ScrollPane panelDeslizable = new ScrollPane();

    private VBox panelBuscador = new VBox();

    private Button botonVerHoteles = new Button("Ver Hoteles");
    private Button botonResgresar = new Button("Regresar");
    private Button regresar = new Button("Regresar");

    private Label labelBusquedaProximidad = new Label("Busqueda por proximidad");
    static TextField textFieldBusquedaProximidad = new TextField();
    private static Button botonBusquedaProximidad = new Button("Buscar");

    private double minWidthTextField = 450;
    private double minHeightTextField = 30;

    private double maxWidthTextField = 500;
    private double maxHeightTextField = 40;
    private String font = "Yu Gothic UI Semibold";
    private int textSize = 18;
    private int textSizeBotones = 13;

    /**
     * Constructor de la clase
     *
     */
    public VentanaHotelesI() {
        root.getChildren().clear();
        seleccionDeBusqueda();
    }

    /**
     * Selecciona la forma de búsqueda (proximidad o provincia)
     *
     */
    private void seleccionDeBusqueda() {
        root.getChildren().clear();
        setFormato();
        root.setSpacing(10);
        Label bienvenida = new Label("Bienvenido/a, " + ProyectoIIParcial.clienteActual.getNombre() + "!");
        bienvenida.setFont(new Font("Yu Gothic UI Semibold", 25));

        root.getChildren().addAll(bienvenida, labelTipoBusqueda, busquedaProximidad, busquedaProvincias);
        busquedaProximidad.setOnAction(e -> busquedaProximidad());
        busquedaProvincias.setOnAction(e -> busquedaProvincias());
        root.setAlignment(Pos.CENTER);
    }

    /**
     * Cambia el formato default
     *
     */
    private void setFormato() {
        labelTipoBusqueda.setFont(new Font(font, textSize));
        busquedaProximidad.setFont(new Font(font, textSizeBotones));
        busquedaProvincias.setFont(new Font(font, textSizeBotones));
        botonVerHoteles.setFont(new Font(font, textSizeBotones));
        botonResgresar.setFont(new Font(font, textSizeBotones));
        regresar.setFont(new Font(font, textSizeBotones));
        botonBusquedaProximidad.setFont(new Font(font, textSizeBotones));
        labelBusquedaProximidad.setFont(new Font(font, textSize));

        BackgroundImage backgroundImagePrincipal = new BackgroundImage(new Image(getClass().getResource("/datos/imagenesVentanas/FondoVentanaHotelesI.png").toExternalForm()), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(10, 10, false, false, true, true));
        Background backgroundPrincipal = new Background(backgroundImagePrincipal);
        root.setBackground(backgroundPrincipal);

        textFieldBusquedaProximidad.setMinSize(minWidthTextField, minHeightTextField);
        textFieldBusquedaProximidad.setMaxSize(maxWidthTextField, maxHeightTextField);

    }

    /**
     * Inicializa la búsqueda de provincias
     *
     */
    private void busquedaProvincias() {
        llenarComboBoxProvincias();
        organizarControles();
    }

    /**
     * Llena el comboBox de provincias
     *
     */
    private void llenarComboBoxProvincias() {
        comboProvincias.setOnAction(e -> mostrarCiudades());
        comboProvincias.getItems().addAll(ProyectoIIParcial.provincias);
    }

    /**
     * Muestra las ciudades disponibles de acuerdo a la provincia
     *
     */
    private void mostrarCiudades() {
        this.panelCiudades.getChildren().clear();
        this.ciudadesComboBox.clear();

        for (Ciudad c : ProyectoIIParcial.ciudades) {
            if (c.getIdProvincia() == ((Provincia) comboProvincias.getValue()).getIdProvincia()) {
                ciudadesComboBox.add(c);
            }
        }
        llenarComboBoxCiudades();
    }

    /**
     * Llena el comboBox de ciudades
     *
     */
    private void llenarComboBoxCiudades() {
        comboCiudades.getItems().clear();
        comboCiudades.getItems().addAll(ciudadesComboBox);
        comboCiudades.setOnAction(e -> mostrarHoteles()); //BARRA HOTELES

        panelCiudades.getChildren().addAll(labelCiudades, comboCiudades);
        panelCiudades.setAlignment(Pos.CENTER);
        try {
            root.getChildren().add(panelCiudades);
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Retorna el root principal
     *
     */
    public VBox getRoot() {
        return root;
    }

    /**
     * Muestra los hoteles de acuerdo a la ciudad y provincia
     *
     */
    private void mostrarHoteles() {
        hotelesCiudad.clear();
        try {
            int idCiudad = ((Ciudad) comboCiudades.getValue()).getIdCiudad();
            for (Hotel h : ProyectoIIParcial.hoteles) {
                if (h.getiDCiudad() == idCiudad) {
                    hotelesCiudad.add(h);
                }
            }
            if (!(hotelesCiudad.isEmpty())) {
                activarBuscador();
            }
        } catch (NullPointerException | IllegalArgumentException e) {
        }
    }

    /**
     * Activa el buscador en el tableView
     *
     */
    private void activarBuscador() {
        panelBuscador.getChildren().clear();
        table.setMaxWidth(780);
        table.setMaxHeight(300);
        table.setItems(null);

        data = FXCollections.observableArrayList(hotelesCiudad);
        table.setEditable(true);

        TableColumn nombreCol = new TableColumn("Nombre Hotel");
        nombreCol.setMinWidth(250);
        nombreCol.setMaxWidth(275);
        nombreCol.setCellValueFactory(new PropertyValueFactory<Hotel, String>("nombreHotel"));

        TableColumn servicioCol = new TableColumn("Servicio");
        servicioCol.setMinWidth(550);
        servicioCol.setMaxWidth(575);
        servicioCol.setCellValueFactory(new PropertyValueFactory<Hotel, String>("serviciosNombres"));

        FilteredList<Hotel> flHotel = new FilteredList(data, p -> true);//Pass the data to a filtered list
        table.setItems(flHotel);//Set the table's items using the filtered list
        table.getColumns().addAll(nombreCol, servicioCol);

        ChoiceBox<String> choiceBox = new ChoiceBox();
        choiceBox.getItems().addAll("Nombre", "Servicio");
        choiceBox.setValue("Nombre");

        TextField textField = new TextField();
        textField.setPromptText("Busca aquí!");
        textField.setOnKeyReleased(keyEvent -> {
            switch (choiceBox.getValue())//Switch on choiceBox value
            {
                case "Nombre":
                    flHotel.setPredicate(p -> p.getNombreHotel().toLowerCase().contains(textField.getText().toLowerCase().trim()));//filter table by first name
                    break;
                case "Servicio":
                    flHotel.setPredicate(p -> p.getServiciosNombres().toLowerCase().contains(textField.getText().toLowerCase().trim()));//filter table by first name
                    break;
            }
        });
        choiceBox.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal)
                -> {
            if (newVal != null) {
                textField.setText("");
                flHotel.setPredicate(null);
            }
        });

        Provincia provinciaSeleccionada = (Provincia) comboProvincias.getValue();
        Ciudad ciudadSeleccionada = (Ciudad) comboCiudades.getValue();
        botonVerHoteles.setOnAction(e -> cambioDeVentana(flHotel, provinciaSeleccionada, ciudadSeleccionada));
        botonResgresar.setOnAction(e -> window.setScene(ProyectoIIParcial.sceneRegistro));

        HBox panelInferior = new HBox(choiceBox, textField, botonVerHoteles, botonResgresar);
        panelInferior.setAlignment(Pos.CENTER);
        panelInferior.setSpacing(20);

        panelBuscador.setSpacing(5);
        panelBuscador.setPadding(new Insets(10, 0, 0, 10));
        panelBuscador.getChildren().addAll(table, panelInferior);
        root.getChildren().add(panelBuscador);
    }

    /**
     * Realiza el cambio de ventana
     *
     * @param flHotel hoteles para realizar el cambio de ventana
     * @param provincia provicia de los hoteles
     * @param ciudad ciudad de los hoteles
     */
    private void cambioDeVentana(List flHotel, Provincia provincia, Ciudad ciudad) {
        sceneVentanaHotelesII = new Scene(new VentanaHotelesII(flHotel, provincia, ciudad).getRoot(), 450, 600);
        window.setScene(sceneVentanaHotelesII);
    }

    /**
     * Organiza los controles
     *
     */
    private void organizarControles() {
        try {
            labelCiudades.setFont(new Font("Yu Gothic UI Semibold", 15));
            labelProvincias.setFont(new Font("Yu Gothic UI Semibold", 15));

            panelProvincias.getChildren().addAll(labelProvincias, comboProvincias);
            panelProvincias.setAlignment(Pos.CENTER);

            root.getChildren().addAll(panelProvincias);
            root.setSpacing(10);
            root.setAlignment(Pos.TOP_CENTER);
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Busca los hoteles cercanos al punto
     *
     */
    private void Busqueda_Proximidad() {
        try {
            GeoCoding a = new GeoCoding("pk.eyJ1IjoiaHRyaXZpbm8iLCJhIjoiY2s1eW12dHRpMGxvbTNwcjdub3lnODZseSJ9.2PxyxFw0GlP9sL3Ab_qu9w");
            String nombre = textFieldBusquedaProximidad.getText();

            Ubicacion u = a.consultar(nombre);

            double latitud = u.getLatitud();
            double longitud = u.getLongitud();
            obtenerHotelesMapa(longitud, latitud);
            System.out.println(hotelesProximos);

            GeneradorHTMLMapBox b = new GeneradorHTMLMapBox("pk.eyJ1IjoiaHRyaXZpbm8iLCJhIjoiY2s1eW12dHRpMGxvbTNwcjdub3lnODZseSJ9.2PxyxFw0GlP9sL3Ab_qu9w", latitud, longitud);
            for (Hotel hotel : hotelesProximos) {
                b.anadirMarcador(hotel.getLongitud(), hotel.getLatitud(), hotel.getNombreHotel());
            }

            b.grabarHTML("src/HTML/pruebaMarker.html");

            Desktop.getDesktop().browse(Paths.get("src/HTML/pruebaMarker.html").toAbsolutePath().toUri());
        } catch (IOException ex) {
        }

    }

    /**
     * Obtiene el mapa de hoteles en la búsqueda de proximidad
     *
     * @param longitud longitud del punto de búsqueda
     * @param latitud latitud del punto de búsqueda
     */
    private void obtenerHotelesMapa(double longitud, double latitud) {
        hotelesProximos.clear();
        double cte = 0.01;
        for (Hotel hotel : ProyectoIIParcial.hoteles) {
            if (hotel.getLatitud() <= latitud + cte && hotel.getLatitud() >= latitud - cte) {
                if (hotel.getLongitud() <= longitud + cte && hotel.getLongitud() >= longitud - cte) {
                    hotelesProximos.add(hotel);
                }
            }
        }
    }

    /**
     * Busca los hoteles próximos al lugar de interés
     *
     */
    private void busquedaProximidad() {
        root.getChildren().clear();
        root.getChildren().addAll(labelBusquedaProximidad, textFieldBusquedaProximidad, botonBusquedaProximidad);
        root.setAlignment(Pos.CENTER);

        botonBusquedaProximidad.setOnAction(e -> {
            try {
                String busqueda = textFieldBusquedaProximidad.getText();
                if (busqueda.length() == 0) {
                    throw new Exception("No se ha ingresado texto");
                } else {
                    Busqueda_Proximidad();
                    sceneVentanaHotelesII = new Scene(new VentanaHotelesII(hotelesProximos, null, null).getRoot(), 450, 600);
                    window.setScene(sceneVentanaHotelesII);
                }
            } catch (Exception ex) {
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setTitle(ex.getMessage());
                a.setHeaderText("Revise los datos ingresados");
                a.setContentText("Ooops, there was an error!");
                a.showAndWait();
            }

        });
        root.getChildren().add(regresar);
        regresar.setOnAction(e -> seleccionDeBusqueda());
    }
}