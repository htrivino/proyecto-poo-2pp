package GUI;

import Principal.ProyectoIIParcial;
import static Principal.ProyectoIIParcial.sceneVentanaHotelesI;
import static Principal.ProyectoIIParcial.sceneVentanaInformacion;
import static Principal.ProyectoIIParcial.sceneVentanaReserva;
import static Principal.ProyectoIIParcial.window;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import recopilardatos.Ciudad;
import recopilardatos.Hotel;
import recopilardatos.Provincia;
import java.awt.Desktop;
import java.io.IOException;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

/**
 * Esta clase muestra la ventanaII para buscar hoteles
 *
 * @author Grupo8P4
 */
public class VentanaHotelesII {

    private VBox root = new VBox();

    private Label labelProvincias;
    private HBox panelProvincias = new HBox();

    private Label labelCiudades;
    private Label bienvenida = new Label("");

    private HBox panelCiudades = new HBox();

    private static List<Hotel> hotelesCiudad = new ArrayList<>();

    private static ScrollPane panelDeslizable = new ScrollPane();
    private static Button botonRegresar = new Button("Regresar");

    /**
     * Constructor de la clase
     *
     * @param flHotel lista de hoteles para presentar
     * @param provincia provincia de los hoteles
     * @param ciudad ciudad de los hoteles
     */
    public VentanaHotelesII(List flHotel, Provincia provincia, Ciudad ciudad) {
        setFormato();
        bienvenida.setText("Bienvenido, " + ProyectoIIParcial.clienteActual + "!");
        if (provincia == null | ciudad == null) {

            root.getChildren().addAll(bienvenida);
            organizarControles(null, null);
            llenarHoteles(flHotel, ciudad, provincia);

        } else {

            labelProvincias = new Label("Usted seleccionó la provincia: " + provincia);
            labelProvincias.setFont(new Font("Yu Gothic UI Semibold", 15));

            labelCiudades = new Label("Usted seleccionó la ciudad: " + ciudad);
            labelCiudades.setFont(new Font("Yu Gothic UI Semibold", 15));

            root.getChildren().addAll(bienvenida, labelProvincias, labelCiudades);

            organizarControles(ciudad, provincia);
            llenarHoteles(flHotel, ciudad, provincia);
        }

    }

    /**
     * Establece el formato de la ventana
     *
     */
    private void setFormato() {
        bienvenida.setFont(new Font("Yu Gothic UI Semibold", 25));

        BackgroundImage backgroundImagePrincipal = new BackgroundImage(new Image(getClass().getResource("/datos/imagenesVentanas/FondoVentanaHotelesII.jpg").toExternalForm()), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(10, 10, false, false, true, true));
        Background backgroundPrincipal = new Background(backgroundImagePrincipal);
        root.setBackground(backgroundPrincipal);

        panelDeslizable.setStyle("-fx-background: transparent;\n -fx-background-color: transparent;\n -fx-background-insets: 0;\n -fx-border-color: transparent;\n -fx-border-width: 0;\n -fx-border-insets: 0;");
        panelDeslizable.setMaxHeight(400);
    }

    /**
     * Organiza los controles
     *
     * @param ciudad ciudad de los hoteles
     * @param provincia provincia de los hoteles
     *
     */
    private void organizarControles(Ciudad ciudad, Provincia provincia) {
        botonRegresar.setOnAction(e -> window.setScene(sceneVentanaHotelesI));
        botonRegresar.setFont(new Font("Yu Gothic UI Semibold", 14));
        root.setAlignment(Pos.CENTER);
        root.setSpacing(10);
    }

    /**
     * Muetra los hoteles que recibe
     *
     * @param listaHoteles hoteles por mostrar
     * @param ciudad ciudad de los hoteles
     * @param provincia provincia de los hoteles
     *
     */
    private void llenarHoteles(List listaHoteles, Ciudad ciudad, Provincia provincia) {
        panelDeslizable.setContent(null);
        VBox panelHoteles = new VBox();
        hotelesCiudad = listaHoteles;

        for (Hotel h : hotelesCiudad) {
            try {
                Label nombreHotel = new Label(h.getNombreHotel());
                nombreHotel.setTextFill(Color.web("#DC143C"));
                nombreHotel.setFont(new Font("Yu Gothic UI Semibold", 14));

                ImageView imagen;
                try {
                    imagen = new ImageView("/imagenes/" + h.getNombreHotel() + ".jpg");
                    imagen.setFitHeight(100);
                    imagen.setFitWidth(140);

                } catch (IllegalArgumentException e) {
                    imagen = new ImageView("/imagenes/NoPic.jpg");
                    imagen.setFitHeight(100);
                    imagen.setFitWidth(140);
                }

                Label direccionHotel = new Label(h.getDireccionHotel());
                direccionHotel.setTextFill(Color.web("#454545"));
                direccionHotel.setFont(new Font("Yu Gothic UI Light", 14));

                Button botonVerMapa = new Button("Ver Mapa");
                botonVerMapa.setFont(new Font("Yu Gothic UI Semibold", 13));
                Button botonInformacion = new Button("+ Información");
                botonInformacion.setFont(new Font("Yu Gothic UI Semibold", 13));
                Button botonReserva = new Button("Reservar");
                botonReserva.setFont(new Font("Yu Gothic UI Semibold", 13));

                botonVerMapa.setOnAction(e -> VerMapa(h, ciudad));
                botonInformacion.setOnAction(e -> cambioDeEscenaInformacion(h));
                botonReserva.setOnAction(e -> cambioDeEscenaReserva(h));

                HBox botones = new HBox();
                botones.getChildren().addAll(botonInformacion, botonVerMapa, botonReserva);
                botones.setSpacing(10);

                VBox detallesHotel = new VBox();
                HBox estrellas = new HBox();

                for (int i = 1; i <= h.getCalificacion(); i++) {
                    ImageView star = new ImageView("/datos/imagenesVentanas/star1.png");
                    star.setFitHeight(20);
                    star.setFitWidth(20);
                    estrellas.getChildren().add(star);
                }
                detallesHotel.getChildren().addAll(nombreHotel, direccionHotel, estrellas, botones);
                detallesHotel.setSpacing(5);

                HBox hotelUnitario = new HBox();
                //imagen
                hotelUnitario.getChildren().addAll(imagen, detallesHotel);

                hotelUnitario.setSpacing(20);
                panelHoteles.getChildren().addAll(hotelUnitario);

                panelHoteles.setSpacing(10);

            } catch (NullPointerException e) {
                System.out.println("Error nulo" + e);
            } catch (RuntimeException e) {
                System.out.println("Error en runtime" + e);
            }

        }
        try {
            panelHoteles.setAlignment(Pos.TOP_CENTER);
            panelDeslizable.setContent(panelHoteles);
            root.getChildren().addAll(panelDeslizable, botonRegresar);

        } catch (IllegalArgumentException e) {
            System.out.println("F" + e);
        }
    }

    /**
     * Presenta la ubicación en el mapa del hotel de interés
     *
     * @param hotel recibe el nombre del hotel
     * @param ciudad recibe la ciudad del hotel
     *
     */
    private void VerMapa(Hotel hotel, Ciudad ciudad) {
        URI myURI;
        try {

            String[] palabras = hotel.getNombreHotel().split(" ");
            String query = "";
            for (int i = 0; i < palabras.length - 2; i++) {
                query += palabras[i] + "%20";
            }
            query += palabras[palabras.length - 1] + ciudad;

            myURI = new URI("https://www.google.com/maps/search/?api=1&query=" + query);
            Desktop.getDesktop().browse(myURI);

        } catch (URISyntaxException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    /**
     * Realiza el cambio de escena de información de hoteles
     *
     * @param hotel recibe el nombre del hotel
     *
     */
    private void cambioDeEscenaInformacion(Hotel h) {
        sceneVentanaInformacion = new Scene(new VentanaInformacion(h).getRoot(), 1000, 600);
        window.setScene(sceneVentanaInformacion);
    }

    /**
     * Realiza el cambio de escena a la reserva
     *
     * @param hotel recibe el nombre del hotel
     *
     */
    private void cambioDeEscenaReserva(Hotel h) {
        sceneVentanaReserva = new Scene(new VentanaReserva(h).getRoot(), 800, 600);
        window.setScene(sceneVentanaReserva);
    }

    /**
     * Retorna el root principal
     *
     */
    public VBox getRoot() {
        return root;
    }
}
