package GUI;

import Principal.ProyectoIIParcial;
import static Principal.ProyectoIIParcial.sceneVentanaHotelesII;
import static Principal.ProyectoIIParcial.window;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import recopilardatos.Catalogo;
import recopilardatos.Habitacion;
import recopilardatos.Hotel;
import recopilardatos.Servicio;

/**
 * Esta clase muestra la información de acuerdo a los hoteles
 *
 * @author Grupo8P4
 */
public class VentanaInformacion {

    private VBox root = new VBox();
    public static List<Habitacion> habitacionesHotel = new ArrayList<>();
    public static List<Servicio> serviciosHotel = new ArrayList<>();

    private Label nombreHotelLabel;
    private Label infoHabitacionesLabel = new Label("Informacion de habitaciones: ");
    private Label tarjetasLabel;
    private Button botonRegresar = new Button("Regresar");
    private ImageView imagen;
    private String font = "Segoe UI Semibold";
    private FlowPane panelDescripción = new FlowPane();

    Label serviciosLabel = new Label("Se ofrecen los siguientes servicios: ");
    private static ScrollPane panelDeslizable = new ScrollPane();

    private Label descripcionHotel;
    private Label webHotel;

    private Label tipoHabitacion = new Label("Tipo");
    private Label tarifaSencilla = new Label("Tarifa sencilla");
    private Label tarifaDoble = new Label("Tarifa Doble");
    private Label tarifaTripe = new Label("Tarifa Triple");

    private FlowPane panelServicios = new FlowPane();

    /**
     * Constructor de la clase
     *
     * @param hotel recibe el objeto hotel
     */
    public VentanaInformacion(Hotel hotel) {
        organizarControles(hotel);
    }

    /**
     * Constructor de la clase
     *
     * @param hotel recibe el hotel para obtener más información
     *
     */
    private void organizarControles(Hotel hotel) {
        root.getChildren().clear();
        habitacionesHotel.clear();
        serviciosHotel.clear();

        setFormatoVentana(hotel);
        root.getChildren().addAll(nombreHotelLabel, imagen);

        for (Habitacion habitacion : ProyectoIIParcial.habitaciones) {
            if (habitacion.getIdHotel() == hotel.getIdHotel()) {
                habitacionesHotel.add(habitacion);
            }
        }

        if (!(habitacionesHotel.isEmpty())) {
            llenarPanelHabitaciones();
        }

        for (Servicio servicio : ProyectoIIParcial.servicios) {
            if (servicio.getIdHotel() == hotel.getIdHotel()) {
                serviciosHotel.add(servicio);
            }
        }
        if (!(serviciosHotel.isEmpty())) {
            llenarPanelServicios();
        }

        String[] palabras = hotel.getDescripcionHotel().split(" ");
        for (String palabra : palabras) {
            descripcionHotel = new Label(palabra + " ");
            descripcionHotel.setFont(new Font(font, 15));
            panelDescripción.getChildren().add(descripcionHotel);
        }

        botonRegresar.setOnAction(e -> window.setScene(sceneVentanaHotelesII));

        root.getChildren().addAll(tarjetasLabel, panelDescripción, webHotel, botonRegresar);
        root.setSpacing(5);
        root.setAlignment(Pos.CENTER);
    }

    /**
     * Presenta el panel de servicios
     *
     */
    private void llenarPanelServicios() {

        for (Servicio servicio : serviciosHotel) {
            int idServicio = servicio.getIdServicio();
            for (Catalogo catalogo : ProyectoIIParcial.catalogos) {
                if (idServicio == catalogo.getIdServicio()) {
                    Label servicioLabel = new Label(catalogo.getServicio());
                    servicioLabel.setFont(new Font(font, 15));
                    panelServicios.getChildren().add(servicioLabel);
                }
            }
        }
        root.getChildren().addAll(serviciosLabel, panelServicios);
    }

    /**
     * Presenta el panel de habitaciones
     *
     */
    private void llenarPanelHabitaciones() {
        VBox columna1 = new VBox();
        HBox panelHabitaciones = new HBox();
        int espacioFila1 = 10;
        columna1.setSpacing(espacioFila1);
        columna1.setAlignment(Pos.CENTER);

        columna1.getChildren().addAll(tipoHabitacion, tarifaSencilla, tarifaDoble, tarifaTripe);

        panelHabitaciones.getChildren().addAll(columna1);
        panelHabitaciones.setAlignment(Pos.CENTER);
        panelHabitaciones.setSpacing(20);

        for (Habitacion habitacion : habitacionesHotel) {
            Label tipoHabitacion = new Label(habitacion.getTipoHabitacion());
            tipoHabitacion.setFont(new Font(font, 15));
            Label tarifaSencilla = new Label(String.valueOf(habitacion.getTarifaSencilla()));
            tarifaSencilla.setFont(new Font(font, 15));
            Label tarifaDoble = new Label(String.valueOf(habitacion.getTarifaDoble()));
            tarifaDoble.setFont(new Font(font, 15));
            Label tarifaTriple = new Label(String.valueOf(habitacion.getTarifaTriple()));
            tarifaTriple.setFont(new Font(font, 15));

            VBox columna = new VBox(tipoHabitacion, tarifaSencilla, tarifaDoble, tarifaTriple);
            columna.setAlignment(Pos.CENTER);
            columna.setSpacing(espacioFila1);

            panelHabitaciones.getChildren().add(columna);
        }

        root.getChildren().add(panelHabitaciones);
    }

    /**
     * Realiza los cambios de formato en la ventana
     *
     * @param hotel recibe el nombre del hotel
     */
    private void setFormatoVentana(Hotel hotel) {
        tarjetasLabel = new Label(hotel.getTarjetaHotel());
        webHotel = new Label(hotel.getWebHotel());
        nombreHotelLabel = new Label(hotel.getNombreHotel());
        nombreHotelLabel.setFont(new Font(font, 30));
        nombreHotelLabel.setTextFill(Color.web("#DC143C"));

        try {
            imagen = new ImageView("/imagenes/" + hotel.getNombreHotel() + ".jpg");
            imagen.setFitHeight(150);
            imagen.setFitWidth(210);

        } catch (IllegalArgumentException e) {
            System.out.println("Error ilegal" + e);
            imagen = new ImageView("/imagenes/NoPic.jpg");
            imagen.setFitHeight(150);
            imagen.setFitWidth(210);
        }

        infoHabitacionesLabel.setFont(new Font(font, 15));
        infoHabitacionesLabel.setTextFill(Color.web("#8B008B"));

        serviciosLabel.setFont(new Font(font, 15));
        serviciosLabel.setTextFill(Color.web("#8B008B"));

        tipoHabitacion.setFont(new Font(font, 15));
        tipoHabitacion.setTextFill(Color.web("#8B008B"));
        tarifaSencilla.setFont(new Font(font, 15));
        tarifaSencilla.setTextFill(Color.web("#8B008B"));
        tarifaDoble.setFont(new Font(font, 15));
        tarifaDoble.setTextFill(Color.web("#8B008B"));
        tarifaTripe.setFont(new Font(font, 15));
        tarifaTripe.setTextFill(Color.web("#8B008B"));

        webHotel.setFont(new Font(font, 15));
        webHotel.setTextFill(Color.web("#8B008B"));

        tarjetasLabel.setFont(new Font(font, 15));
        tarjetasLabel.setTextFill(Color.web("#8B008B"));

        panelDescripción.setAlignment(Pos.CENTER);

        panelServicios.setHgap(20);
        panelServicios.setAlignment(Pos.CENTER);

        BackgroundImage backgroundImageVentana = new BackgroundImage(new Image(getClass().getResource("/datos/imagenesVentanas/FondoReservaAceptada.png").toExternalForm()), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(10, 10, false, false, true, true));
        Background backgroundVentana = new Background(backgroundImageVentana);

        root.setBackground(backgroundVentana);
    }

    /**
     * Retorna la raíz principal
     *
     */
    public VBox getRoot() {
        return root;
    }

}
