package GUI;

import Principal.Cliente;
import Principal.ProyectoIIParcial;
import static Principal.ProyectoIIParcial.sceneVentanaHotelesI;
import static Principal.ProyectoIIParcial.sceneVentanaVerReservas;
import static Principal.ProyectoIIParcial.window;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 * Esta clase muestra la ventana del registro de datos del usuario
 *
 * @author Grupo8P4
 */
public class VentanaRegistro {
    private VBox root = new VBox();

    private Label nombresLabel = new Label("Ingrese sus nombres: ");
    private static TextField nombresTextField = new TextField();

    private Label apellidosLabel = new Label("Ingrese sus apellidos: ");
    private static TextField apellidosTextField = new TextField();

    private Label cedulaLabel = new Label("Ingrese su número de cédula: ");
    static TextField cedulaTextField = new TextField();

    private Label direccionLabel = new Label("Ingrese su dirección: ");
    private static TextField direccionTextField = new TextField();

    private Label emailLabel = new Label("Ingrese su email: ");
    private static TextField emailTextField = new TextField();

    private HBox panelBotones = new HBox();
    private HBox panelNombresApellidos = new HBox();
    private VBox panelNombres = new VBox();
    private VBox panelApellidos = new VBox();

    private Button botonIngresarDatos = new Button("Ingresar Datos");
    private Button botonSalir = new Button("Salir");
    private Button botonVerReservas = new Button("Ver Reservas");

    private double minWidthTextField = 450;
    private double minHeightTextField = 30;

    private double maxWidthTextField = 500;
    private double maxHeightTextField = 40;
    private String font = "Segoe UI Semibold";
    private int textSize = 18;

    /**
     * Constructor de la clase
     *
     */
    public VentanaRegistro() {
        organizarControles();
    }

    /**
     * Constructor de la clase
     *
     */
    private void organizarControles() {
        setFormato();
        panelNombres.getChildren().addAll(nombresLabel, nombresTextField);
        panelNombres.setAlignment(Pos.CENTER);

        panelApellidos.getChildren().addAll(apellidosLabel, apellidosTextField);
        panelApellidos.setAlignment(Pos.CENTER);

        panelNombresApellidos.getChildren().addAll(panelNombres, panelApellidos);
        panelNombresApellidos.setAlignment(Pos.CENTER);
        panelNombresApellidos.setSpacing(50);

        root.getChildren().addAll(panelNombresApellidos, cedulaLabel, cedulaTextField, direccionLabel, direccionTextField, emailLabel, emailTextField, panelBotones);
        root.setSpacing(10);
        root.setAlignment(Pos.CENTER);

        botonIngresarDatos.setOnAction(e -> {
            ingresarDatos(nombresTextField, apellidosTextField, cedulaTextField, direccionTextField, emailTextField);
        });
        botonSalir.setOnAction(e -> Platform.exit());
        botonVerReservas.setOnAction(e -> {
            sceneVentanaVerReservas = new Scene(new VentanaVerReservas().getRoot(), 600, 500);
            window.setScene(sceneVentanaVerReservas);
        });

    }

    /**
     * Ingresa los datos del usuario
     *
     * @param nombresField dato del usuario
     * @param apellidosField dato del usuario
     * @param cedulaField dato del usuario
     * @param direccionField dato del usuario
     * @param emailField dato del usuario
     *
     */
    private void ingresarDatos(TextField nombresField, TextField apellidosField, TextField cedulaField, TextField direccionField, TextField emailField) {
        try {
            String cedula = cedulaField.getText();
            if (cedula.length() != 10) {
                throw new Exception("Número de cédula incorrecto");
            }

            String nombres = nombresField.getText();
            String apellidos = apellidosField.getText();
            String direccion = direccionField.getText();
            String email = emailField.getText();

            ProyectoIIParcial.clienteActual = new Cliente(nombres, apellidos, direccion, email);
            ProyectoIIParcial.clientes.put(cedula, ProyectoIIParcial.clienteActual);
            //System.out.println(ProyectoIIParcial.clienteActual);
            ProyectoIIParcial.serializar("clientes");

            nombresTextField.clear();
            apellidosTextField.clear();

            cedulaTextField.clear();
            direccionTextField.clear();
            emailTextField.clear();

            sceneVentanaHotelesI = new Scene(new VentanaHotelesI().getRoot(), 800, 600);
            window.setScene(sceneVentanaHotelesI);

        } catch (NumberFormatException e) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error al igresar la cédula");
            a.setHeaderText("Revise los datos ingresados");
            a.setContentText("Ooops, there was an error!");
            a.showAndWait();
        } catch (Exception e) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle(e.getMessage());
            a.setHeaderText("Revise los datos ingresados");
            a.setContentText("Ooops, there was an error!");
            a.showAndWait();
        }
    }

    /**
     * Cambia el formato de la ventana
     *
     */
    private void setFormato() {
        nombresLabel.setFont(new Font(font, textSize));
        apellidosLabel.setFont(new Font(font, textSize));

        cedulaLabel.setFont(new Font(font, textSize));
        direccionLabel.setFont(new Font(font, textSize));
        emailLabel.setFont(new Font(font, textSize));

        nombresTextField.setMinSize(minWidthTextField / 2, minHeightTextField);
        nombresTextField.setMaxSize(maxWidthTextField / 2, maxHeightTextField);

        apellidosTextField.setMinSize(minWidthTextField / 2, minHeightTextField);
        apellidosTextField.setMaxSize(maxWidthTextField / 2, maxHeightTextField);

        cedulaTextField.setMinSize(minWidthTextField, minHeightTextField);
        cedulaTextField.setMaxSize(maxWidthTextField, maxHeightTextField);

        direccionTextField.setMinSize(minWidthTextField, minHeightTextField);
        direccionTextField.setMaxSize(maxWidthTextField, maxHeightTextField);

        emailTextField.setMinSize(minWidthTextField, minHeightTextField);
        emailTextField.setMaxSize(maxWidthTextField, maxHeightTextField);

        botonSalir.setMinSize(180, 40);
        botonSalir.setMaxSize(200, 60);

        botonIngresarDatos.setMinSize(180, 40);
        botonIngresarDatos.setMaxSize(200, 60);

        botonVerReservas.setMaxSize(150, 60);
        botonVerReservas.setMinSize(100, 40);

        botonSalir.setFont(new Font(font, textSize));
        botonVerReservas.setFont(new Font(font, textSize));
        botonIngresarDatos.setFont(new Font(font, textSize));

        panelBotones.setAlignment(Pos.CENTER);
        panelBotones.setSpacing(20);
        panelBotones.getChildren().addAll(botonIngresarDatos, botonVerReservas, botonSalir);

        //Fondo de pantalla
        BackgroundImage backgroundImagePrincipal = new BackgroundImage(new Image(getClass().getResource("/datos/imagenesVentanas/FondoVentanaRegistro.jpg").toExternalForm()), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(10, 10, false, false, true, true));
        Background backgroundPrincipal = new Background(backgroundImagePrincipal);
        root.setBackground(backgroundPrincipal);

        BackgroundImage backgroundImageBotones = new BackgroundImage(new Image(getClass().getResource("/datos/imagenesVentanas/BackgroundBotones.jpg").toExternalForm()), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(10, 10, false, false, true, true));
        Background backgroundBotones = new Background(backgroundImageBotones);
        botonSalir.setBackground(backgroundBotones);
        botonIngresarDatos.setBackground(backgroundBotones);

    }

    /**
     * Retorna el root de la ventana registro
     *
     */
    public VBox getRoot() {
        return root;
    }
}
