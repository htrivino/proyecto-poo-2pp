
package GUI;

import Principal.ProyectoIIParcial;
import static Principal.ProyectoIIParcial.clienteActual;
import static Principal.ProyectoIIParcial.sceneVentanaVerReservas;
import static Principal.ProyectoIIParcial.window;
import Principal.Reserva;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import recopilardatos.Habitacion;
import recopilardatos.Hotel;

/**
 * Esta clase muestra la ventana para las reservaciones
 *
 * @author Grupo8P4
 */
public class VentanaReserva {

    private VBox root = new VBox();
    public static List<Habitacion> habitacionesHotel = new ArrayList<>();

    private Label nombreHotelLabel = new Label("");
    private Label bienvenida = new Label("");

    private HBox panelFecha = new HBox();
    private Label fechaEntrada = new Label("Desde:");
    private TextField fechaEntradaIngresada = new TextField();
    private Label fechaSalida = new Label("Hasta:");
    private TextField fechaSalidaIngresada = new TextField();

    private HBox panelTipoHabitacion = new HBox();
    private Label habitacion = new Label("Seleccione el tipo de habitacion:");
    private ComboBox ComboBoxTipoHabitaciones = new ComboBox();

    private HBox panelTarifaHabitacion = new HBox();
    private Label Tarifahabitacion = new Label("Seleccionar tarifa de la habitacion");
    private ComboBox ComboBoxTarifaHabitaciones = new ComboBox();

    private HBox panelN_habitaciones = new HBox();
    private Label n_habitaciones = new Label("Ingresar numero de habitaciones");
    private TextField n_habitacion = new TextField();

    private HBox panelPrecio = new HBox();
    private Label precio = new Label("Valor total a pagar: ");
    private Label signoDolar = new Label("$");
    private Label precioFinal = new Label("0");
    private Double total;
    private Button botonCalcularCostos = new Button("Calcular costos");

    private HBox panelBotones = new HBox();
    private Button botonReservar = new Button("Reservar");
    private Button botonRegresar = new Button("Regresar");
    private Button verReservas = new Button("Ver Reservas");

    private String fuente = "Yu Gothic UI Semibold";
    private ImageView imagen;

    /**
     * Constructor de la clase
     *
     * @param hotel recibe el nombre del hotel para realizar la reserva
     */
    public VentanaReserva(Hotel hotel) {
        organizarControles(hotel);
        llenarComboTipo();
    }

    /**
     * Constructor de la clase
     *
     * @param hotel recibe el nombre del hotel para realizar la reserva
     *
     */
    private void organizarControles(Hotel hotel) {
        setFormato();
        for (Habitacion habitacion : ProyectoIIParcial.habitaciones) {
            if (habitacion.getIdHotel() == hotel.getIdHotel()) {
                habitacionesHotel.add(habitacion);
            }
        }
        bienvenida.setText(ProyectoIIParcial.clienteActual.getNombre() + ",listo/a para reservar?");

        nombreHotelLabel.setText("Has seleccionó el hotel: " + hotel.getNombreHotel());

        panelFecha.getChildren().addAll(fechaEntrada, fechaEntradaIngresada, fechaSalida, fechaSalidaIngresada);
        panelTipoHabitacion.getChildren().addAll(habitacion, ComboBoxTipoHabitaciones);
        panelTarifaHabitacion.getChildren().addAll(Tarifahabitacion, ComboBoxTarifaHabitaciones);
        panelN_habitaciones.getChildren().addAll(n_habitaciones, n_habitacion);

        panelPrecio.getChildren().addAll(precio, signoDolar, precioFinal);

        botonRegresar.setOnAction(e -> window.setScene(ProyectoIIParcial.sceneVentanaHotelesII));
        verReservas.setOnAction(e
                -> {
            sceneVentanaVerReservas = new Scene(new VentanaVerReservas().getRoot(), 600, 500);
            window.setScene(sceneVentanaVerReservas);
        }
        );
        botonCalcularCostos.setOnAction(e -> totalPagar());
        botonReservar.setOnAction(e -> crearReserva(hotel));
//        botoncalcular.setOnAction(e -> totalPagar());
        root.getChildren().addAll(bienvenida, nombreHotelLabel, panelFecha, panelTipoHabitacion, panelTarifaHabitacion, panelN_habitaciones, panelPrecio);
        panelBotones.getChildren().addAll(botonRegresar, botonReservar, verReservas);

        root.getChildren().addAll(botonCalcularCostos, panelBotones);
        root.setAlignment(Pos.CENTER);
        root.setSpacing(20);

    }

    /**
     * Establece el formato de la ventana
     *
     */
    private void setFormato() {
        fechaEntradaIngresada.setPromptText("dd/mm/yyyy");
        fechaSalidaIngresada.setPromptText("dd/mm/yyyy");
        n_habitacion.setPromptText("Ingrese un valor entero");

        bienvenida.setFont(new Font(fuente, 25));
        fechaEntrada.setFont(new Font(fuente, 15));
        fechaEntradaIngresada.setFont(new Font(fuente, 15));
        fechaSalida.setFont(new Font(fuente, 15));
        n_habitacion.setFont(new Font(fuente, 15));
        fechaSalidaIngresada.setFont(new Font(fuente, 15));
        habitacion.setFont(new Font(fuente, 15));
        Tarifahabitacion.setFont(new Font(fuente, 15));
        n_habitaciones.setFont(new Font(fuente, 15));
        precio.setFont(new Font(fuente, 15));
        signoDolar.setFont(new Font(fuente, 15));
        precioFinal.setFont(new Font(fuente, 15));

        botonRegresar.setFont(new Font(fuente, 15));
        botonReservar.setFont(new Font(fuente, 15));
        verReservas.setFont(new Font(fuente, 15));
        botonCalcularCostos.setFont(new Font(fuente, 15));

        nombreHotelLabel.setFont(new Font(fuente, 15));

        panelPrecio.setSpacing(10);
        panelPrecio.setAlignment(Pos.CENTER);

        panelFecha.setSpacing(10);
        panelFecha.setAlignment(Pos.CENTER);

        panelTipoHabitacion.setSpacing(10);
        panelTipoHabitacion.setAlignment(Pos.CENTER);

        panelTarifaHabitacion.setSpacing(10);
        panelTarifaHabitacion.setAlignment(Pos.CENTER);

        panelN_habitaciones.setSpacing(10);
        panelN_habitaciones.setAlignment(Pos.CENTER);

        panelBotones.setSpacing(20);
        panelBotones.setAlignment(Pos.CENTER);

        BackgroundImage backgroundImagePrincipal = new BackgroundImage(new Image(getClass().getResource("/datos/imagenesVentanas/FondoVentanaReservacion.jpg").toExternalForm()), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(10, 10, false, false, true, true));
        Background backgroundPrincipal = new Background(backgroundImagePrincipal);
        root.setBackground(backgroundPrincipal);

    }

    /**
     * Llena el combo del tipo para la búsqueda
     *
     */
    private void llenarComboTipo() {
        ComboBoxTipoHabitaciones.setOnAction(e -> llenarComboTarifa());
        for (Habitacion h : habitacionesHotel) {
            ComboBoxTipoHabitaciones.getItems().addAll(h.getTipoHabitacion());
        }
    }

    /**
     * A partir del tipo se llena la tarifa
     */
    private void llenarComboTarifa() {
        ComboBoxTarifaHabitaciones.getItems().clear();
        String s = (String) ComboBoxTipoHabitaciones.getValue();
        for (Habitacion h : habitacionesHotel) {
            if (s == h.getTipoHabitacion()) {
                ComboBoxTarifaHabitaciones.getItems().addAll(h.getTarifaSencilla(), h.getTarifaDoble(), h.getTarifaTriple());
            }
        }

    }

    /**
     * Calcula el total a pagar de acuerdo a la reserva realizada
     *
     */
    private void totalPagar() {
        try {
            Date fechaEntrada = ParseFecha(fechaEntradaIngresada.getText());
            Date fechaSalida = ParseFecha(fechaSalidaIngresada.getText());
            long difference = fechaSalida.getTime() - fechaEntrada.getTime();
            double nDias = (difference / (1000 * 60 * 60 * 24));
            Calendar calendar = Calendar.getInstance();
            //Date today = java.time.LocalDate.now();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String date = formatter.format(calendar.getTime());

            Date fechaHoy = ParseFecha(date);
            System.out.println(fechaHoy);
            System.out.println(fechaEntrada);
            long differenceHoy = fechaEntrada.getTime() - fechaHoy.getTime();

            double nDiasHoy = (differenceHoy / (1000 * 60 * 60 * 24));

            if (nDias <= 0 | nDiasHoy <= 0) {
                throw new Exception("Error al ingresar las fechas");
            }

            double tarifa = (double) ComboBoxTarifaHabitaciones.getValue();
            double n_Hab = Double.parseDouble(n_habitacion.getText());

            total = tarifa * n_Hab * nDias;
            DecimalFormat df = new DecimalFormat("####0.00");

            precioFinal.setText(String.valueOf(df.format(total)));

        } catch (Exception e) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle(e.getLocalizedMessage());
            a.setHeaderText("Revise los datos ingresados");
            a.setContentText("Ooops, there was an error!");
            a.showAndWait();
        }

    }

    /**
     * Crea la reserva de acuerdo al hotel recibido
     *
     */
    private void crearReserva(Hotel hotel) {
        try {
            root.getChildren().clear();

            Date fechaEntradaReserva = ParseFecha(fechaEntradaIngresada.getText());
            Date fechaSalidaReserva = ParseFecha(fechaSalidaIngresada.getText());

            int n_Hab = Integer.parseInt(n_habitacion.getText());
            Reserva reserva = new Reserva(clienteActual, hotel, fechaEntradaReserva, fechaSalidaReserva, n_Hab, total, total * 0.1);
            ProyectoIIParcial.reservas.add(reserva);
            System.out.println("La agencia obtuvo una comision de: " + reserva.getComision());

            Label confirmacion = new Label("Su reservación se ha realizado con éxito");
            confirmacion.setFont(new Font("Yu Gothic UI Semibold", 25));

            root.getChildren().addAll(confirmacion, botonRegresar);

            root.setAlignment(Pos.CENTER);
            root.setSpacing(10);
            ProyectoIIParcial.serializar("reservas");

        } catch (NumberFormatException e) {
            System.out.println("Ingresar bien lo datos");
        }
    }

    /**
     * Serializa las reservas
     *
     */
    private void serealizarReservas() {
        try (ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("src/datos/reservas.txt"))) {
            o.writeObject(o);
            System.out.println("Serealizacion completa");
        } catch (IOException ex) {
            System.out.println("No se pudo serealizar " + ex);;
        }
    }

    /**
     * Realiza el cambio de formato de String a Fecha
     *
     */
    public static Date ParseFecha(String fecha) {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } catch (ParseException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            //a.setTitle("Error al introducir la fecha");
            a.setHeaderText("Revise los datos ingresados");
            a.setContentText("Ooops, there was an error!");
            a.showAndWait();
        }
        return fechaDate;
    }

    /**
     * Retorna la raíz de la ventana
     *
     */
    public VBox getRoot() {
        return root;
    }
}
