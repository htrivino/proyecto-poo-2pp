/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Principal.ProyectoIIParcial;
import static Principal.ProyectoIIParcial.sceneRegistro;
import Principal.Reserva;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Esta clase muestra la ventana para consultar
 *
 * @author Grupo8P4
 */
public class VentanaVerReservas {

    private VBox root = new VBox();
    private HBox panelBuscador = new HBox();
    private ComboBox comboBoxBusqueda = new ComboBox();
    private TextField textFieldBusqueda = new TextField();
    private Button botonBuscar = new Button("Buscar");
    private Button botonMostrarTodasReservas = new Button("Mostrar todas las reservas");
    private HBox panelBotones = new HBox();

    private HBox reservaUni = new HBox();
    private Button regresar = new Button("Regresar");

    private VBox misReservas = new VBox();
    private GridPane grid = new GridPane();
    private ScrollPane panelReserva = new ScrollPane();

    private List<Reserva> busquedaReservas = new ArrayList<>();
    private String fuente = "Yu Gothic UI Semibold";

    /**
     * Constructor de la clase
     *
     */
    public VentanaVerReservas() {
        LlenarCombo();
        organizarControles();
    }

    /**
     * Organiza los controles de la ventana
     *
     */
    private void organizarControles() {
        setFormato();
        comboBoxBusqueda.setOnAction(e -> {
            String metodo = ((String) comboBoxBusqueda.getValue());
            if (metodo.equals("Nombre Cliente")) {
                textFieldBusqueda.setPromptText("Ingrese el nombre del cliente");
            }
            if (metodo.equals("Hotel")) {
                textFieldBusqueda.setPromptText("Ingrese el nombre del hotel");
            }
            if (metodo.equals("Fecha")) {
                textFieldBusqueda.setPromptText("Ingrese la fecha dd/mm/yyyy");
            }
        });
        panelReserva.setContent(misReservas);
        grid.getChildren().add(panelReserva);
        grid.setAlignment(Pos.TOP_CENTER);
        regresar.setOnAction(e -> ProyectoIIParcial.window.setScene(sceneRegistro));
        panelBotones.getChildren().addAll(regresar, botonMostrarTodasReservas);
        botonMostrarTodasReservas.setOnAction(e -> {
            presentarReservas(ProyectoIIParcial.reservas);
        });
        panelBuscador.getChildren().addAll(comboBoxBusqueda, textFieldBusqueda, botonBuscar);
        root.getChildren().addAll(panelBuscador, grid, panelBotones);
        botonBuscar.setOnAction(e
                -> {
            buscarReservas();
        });
    }

    /**
     * Establece el formato de la ventana
     *
     */
    private void setFormato() {
        root.setAlignment(Pos.CENTER);
        root.setSpacing(10);

        textFieldBusqueda.setPromptText("Buscar aqui");
        textFieldBusqueda.setFont(new Font(fuente, 15));

        botonBuscar.setFont(new Font(fuente, 15));
        botonMostrarTodasReservas.setFont(new Font(fuente, 15));
        regresar.setFont(new Font(fuente, 15));

        panelBuscador.setSpacing(10);
        panelBuscador.setAlignment(Pos.CENTER);

        textFieldBusqueda.setMinSize(300, 20);
        textFieldBusqueda.setMaxSize(350, 40);

        panelBotones.setSpacing(10);
        panelBotones.setAlignment(Pos.CENTER);

        reservaUni.setSpacing(10);
        reservaUni.setAlignment(Pos.CENTER);

        misReservas.setSpacing(10);
        reservaUni.setAlignment(Pos.CENTER);

        BackgroundImage backgroundImagePrincipal = new BackgroundImage(new Image(getClass().getResource("/datos/imagenesVentanas/FondoVentanaReservacion.jpg").toExternalForm()), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(10, 10, false, false, true, true));
        Background backgroundPrincipal = new Background(backgroundImagePrincipal);
        root.setBackground(backgroundPrincipal);

        panelReserva.setStyle("-fx-background: transparent;\n -fx-background-color: transparent;\n -fx-background-insets: 0;\n -fx-border-color: transparent;\n -fx-border-width: 0;\n -fx-border-insets: 0;");
        panelReserva.setMaxHeight(400);

    }

    /**
     * Llena el combo de búsqueda
     *
     */
    private void LlenarCombo() {
        comboBoxBusqueda.getItems().addAll("Fecha", "Hotel", "Nombre Cliente");
    }

    /**
     * Retorna el root
     *
     */
    public VBox getRoot() {
        return root;
    }

    /**
     * Realiza las búsquedas de acuerdo al criterio escogido
     *
     */
    private void buscarReservas() {
        busquedaReservas.clear();
        String metodo = (String) comboBoxBusqueda.getValue();
        String consulta = textFieldBusqueda.getText();
        System.out.println(busquedaReservas);
        try {
            if (consulta.length() == 0) {
                throw new Exception();
            } else {
                if (metodo.equals("Fecha")) {
                    try {
                        VentanaReserva.ParseFecha(consulta);
                        for (Reserva reserva : ProyectoIIParcial.reservas) {
                            Date dateEntrada = reserva.getFechaEntrada();
                            Date dateSalida = reserva.getFechaSalida();
                            DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
                            String fechaEntrada = dateFormat.format(dateEntrada);
                            String fechaSalida = dateFormat.format(dateSalida);
                            if (fechaEntrada.equals(consulta) || fechaSalida.equals(consulta)) {
                                busquedaReservas.add(reserva);
                            }
                        }
                        presentarReservas(busquedaReservas);
                    } catch (Exception e) {
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setTitle(e.getLocalizedMessage());
                        a.setHeaderText("Revise los datos ingresados");
                        a.setContentText("Ooops, there was an error!");
                        a.showAndWait();
                    }
                }
                if (metodo.equals("Hotel")) {
                    try {
                        for (Reserva reserva : ProyectoIIParcial.reservas) {
                            String nombreHotelReserva = reserva.getHotel().toString();
                            if (nombreHotelReserva.contains(consulta)) {
                                busquedaReservas.add(reserva);
                            }
                        }
                        presentarReservas(busquedaReservas);
                    } catch (Exception e) {
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setTitle(e.getLocalizedMessage());
                        a.setHeaderText("Revise los datos ingresados");
                        a.setContentText("Ooops, there was an error!");
                        a.showAndWait();
                    }
                } else {
                    try {
                        for (Reserva reserva : ProyectoIIParcial.reservas) {
                            //String clienteConsulta = textFieldBusqueda.getText();
                            String nombreHotelCliente = reserva.getCliente().toString();
                            if (nombreHotelCliente.contains(consulta)) {
                                busquedaReservas.add(reserva);
                            }
                        }
                        presentarReservas(busquedaReservas);
                    } catch (Exception e) {
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setTitle(e.getLocalizedMessage());
                        a.setHeaderText("Revise los datos ingresados");
                        a.setContentText("Ooops, there was an error!");
                        a.showAndWait();
                    }
                }
            }
        } catch (Exception e) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle(e.getLocalizedMessage());
            a.setHeaderText("Revise los datos ingresados");
            a.setContentText("Ooops, there was an error!");
            a.showAndWait();
        }
    }

    /**
     * Presenta las reservas de acuerdo a la lista recibida
     *
     * @args listaReservas lista de las reservas
     *
     */
    private void presentarReservas(List<Reserva> listaReservas) {
        misReservas.getChildren().clear();
        if (!(listaReservas.isEmpty())) {
            for (Reserva reserva : listaReservas) {
                Label nombreHotel = new Label(reserva.getHotel().getNombreHotel());
                nombreHotel.setTextFill(Color.web("#DC143C"));
                nombreHotel.setFont(new Font("Yu Gothic UI Semibold", 14));

                DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");

                Label labelFechaEntrada = new Label("Fecha entrada: " + dateFormat.format(reserva.getFechaEntrada()));
                labelFechaEntrada.setTextFill(Color.web("#454545"));
                labelFechaEntrada.setFont(new Font("Yu Gothic UI Light", 14));

                Label labelFechaSalida = new Label("Fecha salida: " + dateFormat.format(reserva.getFechaSalida()));
                labelFechaSalida.setTextFill(Color.web("#454545"));
                labelFechaSalida.setFont(new Font("Yu Gothic UI Light", 14));

                Label labelCliente = new Label("Cliente: " + reserva.getCliente());
                labelCliente.setTextFill(Color.web("#454545"));
                labelCliente.setFont(new Font("Yu Gothic UI Light", 14));

                DecimalFormat df = new DecimalFormat("####0.00");
                Label labelPrecio = new Label("Costo total: $" + df.format(reserva.getPrecioTotal()));
                labelPrecio.setTextFill(Color.web("#454545"));
                labelPrecio.setFont(new Font("Yu Gothic UI Light", 14));
                Label espacio = new Label("");
                misReservas.getChildren().addAll(nombreHotel, labelFechaEntrada, labelFechaSalida, labelCliente, labelPrecio, espacio);
            }
            misReservas.setAlignment(Pos.CENTER);
        } else {
            Label noResults = new Label("No se encontraron resultados:(");
            noResults.setTextFill(Color.web("#454545"));
            noResults.setFont(new Font("Yu Gothic UI Semibold", 14));
            misReservas.getChildren().add(noResults);
        }
    }
}
