package Principal;

import java.io.Serializable;

/**
 * Clase Cliente (Implementa serializable)
 *
 * @author Grupo8
 */
public class Cliente implements Serializable {
    private String nombres;
    private String apellidos;
    private String direccion;
    private String email;

    /**
     * Constructor de la clase
     *
     * @param nombes recibe los datos del cliente
     * @param apellidos recibe los datos del cliente
     * @param direccion recibe los datos del cliente
     * @param email recibe los datos del cliente
     */
    public Cliente(String nombres, String apellidos, String direccion, String email) {
        //this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.email = email;
    }

    /**
     * Sobreescritura del método toString
     *
     */
    @Override
    public String toString() {
        return nombres + " " + apellidos;
    }

    /**
     * Getter del nombre del Cliente
     *
     */
    public String getNombre() {
        return nombres;
    }
}
