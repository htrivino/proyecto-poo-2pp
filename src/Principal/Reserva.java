
package Principal;

import java.io.Serializable;
import java.util.Date;
import recopilardatos.Hotel;

/**
 * Esta clase inicializa las reservas
 *
 * @author Grupo8P4
 */
public class Reserva implements Serializable {

    private Cliente cliente;
    private Hotel hotel;
    private Date fechaEntrada;
    private Date fechaSalida;
    private int Nhabitaciones;
    private Double precioTotal;
    private Double comision;

    /**
     * Constructor de la clase
     *
     * @param cliente objeto del cliente que realizó la reserva
     * @param hotel objeto del hotel de la reserva
     * @param fechaEntrada fecha de entrada del usuario
     * @param fechaSalida fecha de salida del usuario
     * @param Nhabitaciones numero de habitaciones de la reserva
     * @param precioTotal precio total de la reserva
     * @param comision total de la comision de ClickTour
     *
     */
    public Reserva(Cliente cliente, Hotel hotel, Date fechaEntrada, Date fechaSalida, int Nhabitaciones, double precioTotal, double comision) {
        this.cliente = cliente;
        this.hotel = hotel;
        this.fechaEntrada = fechaEntrada;
        this.fechaSalida = fechaSalida;
        this.Nhabitaciones = Nhabitaciones;
        this.precioTotal = precioTotal;
        this.comision = comision;
    }

    /**
     * Getter de la comisón
     */
    public Double getComision() {
        return comision;
    }

    /**
     * Getter del precio total
     */
    public Double getPrecioTotal() {
        return precioTotal;
    }

    /**
     * Getter del cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * Getter del hotel
     */
    public Hotel getHotel() {
        return hotel;
    }

    /**
     * Getter de la fecha de entrada
     */
    public Date getFechaEntrada() {
        return fechaEntrada;
    }

    /**
     * Getter de la fecha de salida
     */
    public Date getFechaSalida() {
        return fechaSalida;
    }

    /**
     * Getter de la cantidad de habitaciones reservadas
     */
    public int getNhabitaciones() {
        return Nhabitaciones;
    }

    /**
     * Sobreescritura del método toString
     */
    @Override
    public String toString() {
        return "Reserva{" + "cliente=" + cliente + ", hotel=" + hotel + ", fechaEntrada=" + fechaEntrada + ", fechaSalida=" + fechaSalida + ", N_habitaciones=" + Nhabitaciones + ", precioTotal=" + precioTotal + ", comision=" + comision + '}';
    }
}
