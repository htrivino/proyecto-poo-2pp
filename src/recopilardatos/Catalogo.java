package recopilardatos;

/**
 * Esta clase inicializa el catálogo de servicios
 *
 * @author Grupo8P4
 */
public class Catalogo {

    private int idServicio;
    private String servicio;

    /**
     * Constructor de la clase
     *
     * @param idServicio id del servicio
     * @param servicio nombre del servicio
     */
    public Catalogo(int idServicio, String servicio) {
        this.idServicio = idServicio;
        this.servicio = servicio;
    }

    /**
     * Sobreescritura del método toString
     */
    @Override
    public String toString() {
        return "Catalogo{" + "idServicio=" + idServicio + ", servicio=" + servicio + '}';
    }

    /**
     * Getter del id del servicio
     */
    public int getIdServicio() {
        return idServicio;
    }

    /**
     * getter del nombre del servicio
     */
    public String getServicio() {
        return servicio;
    }
}
