
package recopilardatos;

/**
 * Esta clase inicializa la ciudad
 *
 * @author Grupo8P4
 */
public class Ciudad {
    private int idCiudad;
    private int idProvincia;
    private String ciudad;

    /**
     * Constructor de la clase
     *
     * @param idCiudad id de la ciudad
     * @param idProvincia id de la provincia de la ciudad
     * @param ciudad nombre de la ciudad
     */
    public Ciudad(int idCiudad, int idProvincia, String ciudad) {
        this.idCiudad = idCiudad;
        this.idProvincia = idProvincia;
        this.ciudad = ciudad;
    }

    /**
     * Sobreescritura del método toString
     */
    @Override
    public String toString() {
        return ciudad;
    }

    /**
     * getter del id de la provincia
     */
    public int getIdProvincia() {
        return idProvincia;
    }

    /**
     * getter del id de la ciudad
     */
    public int getIdCiudad() {
        return idCiudad;
    }  
}
