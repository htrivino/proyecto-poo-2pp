package recopilardatos;

/**
 * Esta clase inicializa la habitación
 *
 * @author Grupo8P4
 */
public class Habitacion {
    private int idHabitacion;
    private int idHotel;
    private String tipoHabitacion;
    private double tarifaSencilla;
    private double tarifaDoble;
    private double tarifaTriple;

    /**
     * Constructor de la clase
     *
     * @param idHabitacion id de la habitacion
     * @param idHotel id del hotel
     * @param tipoHabitacion tipo de habitacion
     * @param tarifaSencilla valor de tarifa sencilla
     * @param tarifaDoble valor de tarifa doble
     * @param tarifaTriple valor de tarifa triple
     */
    public Habitacion(int idHabitacion, int idHotel, String tipoHabitacion, double tarifaSencilla, double tarifaDoble, double tarifaTriple) {
        this.idHabitacion = idHabitacion;
        this.idHotel = idHotel;
        this.tipoHabitacion = tipoHabitacion;
        this.tarifaSencilla = tarifaSencilla;
        this.tarifaDoble = tarifaDoble;
        this.tarifaTriple = tarifaTriple;
    }

    /**
     * getter del id del hotel
     */
    public int getIdHotel() {
        return idHotel;
    }

    /**
     * getter del nombre del tipo de habitacion
     */
    public String getTipoHabitacion() {
        return tipoHabitacion;
    }

    /**
     * Sobreescritura del método toString
     */
    @Override
    public String toString() {
        return "Tipo de habitacion: " + tipoHabitacion + ", Tarifa Sencilla: " + tarifaSencilla + ", Tarifa Doble: " + tarifaDoble + ", Tarifa Triple: " + tarifaTriple;
    }

    /**
     * getter del valor de tarifa sencilla
     */
    public double getTarifaSencilla() {
        return tarifaSencilla;
    }

    /**
     * getter de tarifa doble
     */
    public double getTarifaDoble() {
        return tarifaDoble;
    }

    /**
     * getter del tarifa triple
     */
    public double getTarifaTriple() {
        return tarifaTriple;
    }
}
