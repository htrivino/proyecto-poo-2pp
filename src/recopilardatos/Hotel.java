package recopilardatos;

import java.io.Serializable;

/**
 * Esta clase inicializa el hotel
 *
 * @author Grupo8P4
 */
public class Hotel implements Serializable {
    private int idHotel;
    private int iDCiudad;
    private String nombreHotel;
    private String descripcionHotel;
    private String tarjetaHotel;
    private String ubicacionHotel;
    private String direccionHotel;
    private String webHotel;
    private int calificacion;
    private String rutaImagenHotel;
    private double latitud;
    private double longitud;
    private String serviciosNombres = "";

    /**
     * Constructor de la clase
     *
     * @param iDCiudad id de la ciudad donde se encuentra el hotel
     * @param idHotel id del hotel
     * @param nombreHotel nombre del hotel
     * @param descripcionHotel descripción del hotel
     * @param tarjetaHotel tarjetas que acepta el hotel
     * @param ubicacionHotel ubicacion del hotel
     * @param direccionHotel direccion del hotel
     * @param webHotel ubicacion de la web del hotel
     * @param calificacion calificacion en estrellas del hotel
     * @param rutaImagenHotel ubicacion de la imagen del hotel
     * @param latitud ubicacion del hotel
     * @param longitud ubicacion del hotel
     *
     *
     */
    public Hotel(int idHotel, int iDCiudad, String nombreHotel, String descripcionHotel, String tarjetaHotel, String ubicacionHotel, String direccionHotel, String webHotel, int calificacion, String rutaImagenHotel, double latitud, double longitud) {
        this.idHotel = idHotel;
        this.iDCiudad = iDCiudad;
        this.nombreHotel = nombreHotel;
        this.descripcionHotel = descripcionHotel;
        this.tarjetaHotel = tarjetaHotel;
        this.ubicacionHotel = ubicacionHotel;
        this.direccionHotel = direccionHotel;
        this.webHotel = webHotel;
        this.calificacion = calificacion;
        this.rutaImagenHotel = rutaImagenHotel;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    /**
     * getter de la latitud del hotel
     */
    public double getLatitud() {
        return latitud;
    }

    /**
     * getter de la longitud del hotel
     */
    public double getLongitud() {
        return longitud;
    }

    /**
     * setter del String de servicios
     *
     * @param servicios establece los servicios del hotel
     */
    public void setServiciosNombres(String servicios) {
        this.serviciosNombres = servicios;
    }

    /**
     * getter de los nombres de servicios
     */
    public String getServiciosNombres() {
        return serviciosNombres;
    }

    /**
     * getter del id del hotel
     */
    public int getIdHotel() {
        return idHotel;
    }

    /**
     * getter de la descripción del hotel
     */
    public String getDescripcionHotel() {
        return descripcionHotel;
    }

    /**
     * getter de las tarjetas que recibe el hotel
     */
    public String getTarjetaHotel() {
        return tarjetaHotel;
    }

    /**
     * getter de la calificacion del hotel
     */
    public int getCalificacion() {
        return calificacion;
    }

    /**
     * getter de la ruta de la imagen
     */
    public String getRutaImagenHotel() {
        return rutaImagenHotel;
    }

    /**
     * Sobreescritura del método toString
     */
    @Override
    public String toString() {
        return nombreHotel;
    }

    /**
     * getter del id de la ciudad donde se encuentra el hotel
     */
    public int getiDCiudad() {
        return iDCiudad;
    }

    /**
     * getter de la direccion del hotel
     */
    public String getDireccionHotel() {
        return direccionHotel;
    }

    /**
     * getter del sitio web del hotel
     */
    public String getWebHotel() {
        return webHotel;
    }

    /**
     * getter del nombre del hotel
     */
    public String getNombreHotel() {
        return nombreHotel;
    }

    /**
     * setter de la ruta de la imagen del hotel
     */
    public void setRutaImagenHotel(String rutaImagenHotel) {
        this.rutaImagenHotel = rutaImagenHotel;
    }
}
