package recopilardatos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import static Principal.ProyectoIIParcial.catalogos;
import static Principal.ProyectoIIParcial.ciudades;
import static Principal.ProyectoIIParcial.habitaciones;
import static Principal.ProyectoIIParcial.hoteles;
import static Principal.ProyectoIIParcial.provincias;
import static Principal.ProyectoIIParcial.servicios;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * Esta clase realiza la lectura de los archivos
 *
 * @author Grupo8P4
 */
public class Lectura {
    /**
     * Realiza la lectura de distintos archivos
     */
    public static void leerArchivos() {
        lecturaServicio();
        lecturaCatalogo();
        lecturaCiudad();
        lecturaHabitacion();
        lecturaHoteles();
        lecturaProvincia();
        setServiciosHoteles();
    }

    /**
     * lectura de los servicios del doc
     */
    public static void lecturaServicio() {
        try (BufferedReader r = new BufferedReader(new FileReader("src/datos/servicios.csv"))) {
            String l;
            while ((l = r.readLine()) != null) {
                try {
                    String[] linea = l.split("\\|");
                    servicios.add(new Servicio(Integer.parseInt(linea[0]), Integer.parseInt(linea[1]), Integer.parseInt(linea[2]), linea[3]));
                } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                    System.err.println("Error en línea " + l + e);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error " + e);
        } catch (IOException e) {
            System.err.println("Error " + e);
        }
    }

    /**
     * lectura de los catalogo del doc
     */
    public static void lecturaCatalogo() {
        try (BufferedReader r = new BufferedReader(new FileReader("src/datos/catalogo.csv"))) {
            String l;
            while ((l = r.readLine()) != null) {
                try {
                    String[] linea = l.split("\\|");
                    catalogos.add(new Catalogo(Integer.parseInt(linea[0]), linea[1]));
                } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                    System.err.println("Error en línea " + l + e);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error " + e);
        } catch (IOException e) {
            System.err.println("Error " + e);
        }
    }

    /**
     * lectura de las ciudades del doc
     */
    public static void lecturaCiudad() {
        try (BufferedReader r = new BufferedReader(new FileReader("src/datos/ciudades.csv"))) {
            String l;
            while ((l = r.readLine()) != null) {
                try {
                    String[] linea = l.split("\\|");
                    ciudades.add(new Ciudad(Integer.parseInt(linea[0]), Integer.parseInt(linea[1]), linea[2]));
                } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                    System.err.println("Error en línea " + l + e);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error " + e);
        } catch (IOException e) {
            System.err.println("Error " + e);
        }
    }

    /**
     * lectura de las habitaciones del doc
     */
    public static void lecturaHabitacion() {
        try (BufferedReader r = new BufferedReader(new FileReader("src/datos/habitaciones.csv"))) {
            String l;
            while ((l = r.readLine()) != null) {
                try {
                    String[] linea = l.split("\\|");
                    habitaciones.add(new Habitacion(Integer.parseInt(linea[0]), Integer.parseInt(linea[1]), linea[2], Double.parseDouble(linea[3]), Double.parseDouble(linea[4]), Double.parseDouble(linea[5])));
                } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                    System.err.println("Error en línea " + l + e);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error " + e);
        } catch (IOException e) {
            System.err.println("Error " + e);
        }
    }

    /**
     * lectura de los hoteles del doc
     */
    public static void lecturaHoteles() {
        try (BufferedReader r = new BufferedReader(new FileReader("src/datos/hoteles1.csv"))) {
            String l;
            while ((l = r.readLine()) != null) {
                try {
                    String[] linea = l.split("\\|");
                    hoteles.add(new Hotel(Integer.parseInt(linea[0]), Integer.parseInt(linea[1]), linea[2], linea[3], linea[4], linea[5], linea[6], linea[7], Integer.parseInt(linea[8]), linea[9], Double.parseDouble(linea[10]), Double.parseDouble(linea[11])));
                } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                    System.err.println("Error en línea " + l + e);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error " + e);
        } catch (IOException e) {
            System.err.println("Error " + e);
        }
    }

    /**
     * lectura de las provincias del doc
     */
    public static void lecturaProvincia() {
        try (BufferedReader r = new BufferedReader(new FileReader("src/datos/provincias.csv"))) {
            String l;
            while ((l = r.readLine()) != null) {
                try {
                    String[] linea = l.split("\\|");
                    provincias.add(new Provincia(Integer.parseInt(linea[0]), linea[1], linea[2], linea[3], linea[4]));
                } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                    System.err.println("Error en línea " + l + e);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error " + e);
        } catch (IOException e) {
            System.err.println("Error " + e);
        }
    }

    /**
     * establece el cambio de los servicios de los hoteles
     */
    public static void setServiciosHoteles() {
        for (Hotel hotel : hoteles) {
            String cadenaServicios = "";
            ArrayList<String> nombreServiciosList = new ArrayList<>();
            ArrayList<Servicio> serviciosList = new ArrayList<>();

            for (Servicio servicio : servicios) {
                if (servicio.getIdHotel() == hotel.getIdHotel()) {
                    serviciosList.add(servicio);
                }
            }
            for (Servicio servicio : serviciosList) {
                for (Catalogo catalogo : catalogos) {
                    if (catalogo.getIdServicio() == servicio.getIdServicio()) {
                        String nombreServicio = catalogo.getServicio();
                        nombreServiciosList.add(nombreServicio);
                    }
                }
            }
            for (String servicio : nombreServiciosList) {
                cadenaServicios += servicio + " ,";
            }
            hotel.setServiciosNombres(cadenaServicios);

        }
    }

    /**
     * realiza la descarga de imágenes
     */
    public static void DescargaImagenes() {

        for (Hotel h : hoteles) {
            try {
                String website = h.getRutaImagenHotel();

                //System.out.println("Downloading File From: " + website);
                URL url = new URL(website);
                InputStream inputStream = url.openStream();
                String fileName = url.getFile();
                String destName = "src/imagenes/" + h.getNombreHotel() + ".jpg";

                h.setRutaImagenHotel(destName);

                OutputStream outputStream = new FileOutputStream(destName);
                byte[] buffer = new byte[2048];

                int length = 0;

                while ((length = inputStream.read(buffer)) != -1) {
                    System.out.println("Buffer Read of length: " + length);
                    outputStream.write(buffer, 0, length);
                }
                inputStream.close();
                outputStream.close();
            } catch (Exception e) {
                System.out.println("Error");
            }

        }

    }
}
