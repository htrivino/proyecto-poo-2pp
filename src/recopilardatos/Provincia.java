package recopilardatos;

/**
 * Esta clase inicializa la provincia
 *
 * @author Grupo8P4
 */
public class Provincia {
    private int idProvincia;
    private String provincia;
    private String descripcion;
    private String region;
    private String web;

    /**
     * Constructor de la clase
     *
     * @param idProvincia id de la provincia
     * @param provincia nombre de la provincia
     * @param descripcion descripcion de la provincia
     * @param region region de la provincia
     * @param web web de la provincia
     *
     */
    public Provincia(int idProvincia, String provincia, String descripcion, String region, String web) {
        this.idProvincia = idProvincia;
        this.provincia = provincia;
        this.descripcion = descripcion;
        this.region = region;
        this.web = web;
    }

    /**
     * Sobreescritura del método toString
     */
    @Override
    public String toString() {
        return provincia;
    }

    /**
     * getter del id de provincia
     */
    public int getIdProvincia() {
        return idProvincia;
    }
}
