package recopilardatos;

/**
 * Esta clase inicializa el servicio
 *
 * @author Grupo8P4
 */
public class Servicio {

    private int secuencia;
    private int idHotel;
    private int idServicio;
    private String estado;

    /**
     * Constructor de la clase
     *
     * @param secuencia secuencia del servicio
     * @param idHotel id del hotel donde se presta el servicio
     * @param idServicio id del servicio
     * @param estado activo/inactivo
     *
     */
    public Servicio(int secuencia, int idHotel, int idServicio, String estado) {
        this.secuencia = secuencia;
        this.idHotel = idHotel;
        this.idServicio = idServicio;
        this.estado = estado;
    }

    /**
     * getter del id del servicio
     */
    public int getIdServicio() {
        return idServicio;
    }

    /**
     * getter del id del hotel
     */
    public int getIdHotel() {
        return idHotel;
    }

    /**
     * Sobreescritura del método toString
     */
    @Override
    public String toString() {
        return "Servicio{" + "secuencia=" + secuencia + ", idHotel=" + idHotel + ", idServicio=" + idServicio + ", estado=" + estado + '}';
    }
}
